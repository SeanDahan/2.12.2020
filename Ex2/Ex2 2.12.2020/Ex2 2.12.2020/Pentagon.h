#pragma once
#include "shape.h"
#include "shapeexception.h"
#include "MathUtils.h"
class Pentagon : public Shape
{
public:
	Pentagon(std::string nam, std::string col, double newS) : Shape(nam, col) { if (newS < 0) throw shapeException(); setSide(newS); };
	void setSide(double newS) { _side = newS; };
	void draw() { std::cout << "Name is " << getName() << "\nColor is" << getColor() << "\nArea is " << CalArea() << "\n";}; //DEFINE FOR ALL
	double CalArea() { return MathUtils::calPentagonArea(_side);};
private:
	double _side;
	
};

