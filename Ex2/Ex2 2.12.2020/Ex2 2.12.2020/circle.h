#ifndef CIRCLE_H
#define CIRCLE_H
#include "shape.h"
#include <iostream>

class Circle: public Shape {

public:
	void draw();
	double CalArea();
	double CalCircumference();
	Circle(std::string name, std::string color, double);
	void setRad(double rad);
	double getRad();
private:
	double radius;


};
#endif