#include "MathUtils.h"

double MathUtils::calPentagonArea(double side)
{
	return (pow(side, 2) / 4) * sqrt(25 + 10 * sqrt(5));
}

double MathUtils::calHexagonArea(double side)
{
	return 6 * ((pow(side, 2) * sqrt(3)) / 4);
}
