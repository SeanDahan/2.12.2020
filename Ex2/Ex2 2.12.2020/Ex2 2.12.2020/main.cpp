#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include "shapeException.h"
#include "Hexagon.h"
#include "Pentagon.h"

int main()
{
	std::string nam, col;
	double length = 0, rad = 0, ang = 0, ang2 = 180; int height = 0, width = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	Pentagon penta(nam, col, length);
	Hexagon hexa(nam, col, length);

	Shape *ptrpenta = &penta;
	Shape* ptrhexa = &hexa;
	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;


	
	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p'; 
	char shapetype;
	
	char x = 'y';
	while (x != 'x') {
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, x = hexagon, t = pentagon" << std::endl;
		std::cin >> shapetype;
		if (std::cin.rdbuf()->in_avail() != 1)
		{
			//<<<<<<<<<<<<<
			//
			//My workthrough
			//
			//<<<<<<<<<<<<<
			//std::cout << "YAP\n" << "\n" << std::cin.rdbuf()->in_avail();
			//std::cout << std::cin.rdbuf()->in_avail();
			//std::cin.clear(); // put us back in 'normal' operation mode
			//std::cin.rdbuf()->in_avail()
			//std::cin.ignore(32767, '\n');
			printf("Warning - Don't try to build more then one shape at once");
		}
		
try
		{

			switch (shapetype) {
			case 'c':
				std::cout << "enter color, name,  rad for circle" << std::endl;
				
				std::cin >> col >> nam >> rad;
				if (std::cin.fail()) // has a previous extraction failed?
				{
					throw inputException();
				}
				
				circ.setColor(col);
				circ.setName(nam);
				circ.setRad(rad);
				ptrcirc->draw();
				break;
			case 'q':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				if (std::cin.fail()) // has a previous extraction failed?
				{
					throw inputException();
				}
				quad.setName(nam);
				quad.setColor(col);
				quad.setHeight(height);
				quad.setWidth(width);
				ptrquad->draw();
				break;
			case 'r':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				if (std::cin.fail()) // has a previous extraction failed?
				{
					throw inputException();
				}
				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();
				break;
			case 'p':
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				std::cin >> nam >> col >> height >> width >> ang >> ang2;
				if (std::cin.fail()) // has a previous extraction failed?
				{
					throw inputException();
				}
				
				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);
				ptrpara->draw();
			case 'x':
				std::cout << "enter name, color, length" << std::endl;
				std::cin >> nam >> col >> length;
				if (std::cin.fail()) // has a previous extraction failed?
				{
					throw inputException();
				}
				hexa.setName(nam);
				hexa.setColor(col);
				hexa.setSide(length);
				ptrhexa->draw();
				break;
			case 't':
				std::cout << "enter name, color, length" << std::endl;
				std::cin >> nam >> col >> length;
				if (std::cin.fail()) // has a previous extraction failed?
				{
					throw inputException();
				}
				penta.setName(nam);
				penta.setColor(col);
				penta.setSide(length);
				ptrpenta->draw();
				break;
			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			std::cin >> x;
		}
		catch (inputException e)
		{
			printf(e.what());
			printf("\n");  
			std::cin.clear(); // put us back in 'normal' operation mode
			std::cin.ignore(32767, '\n'); // and remove the bad input
		}
		catch (shapeException e)
		{			
			
			printf(e.what());
			printf("\n");
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}



		system("pause");
		return 0;
	
}