#pragma once
#include <math.h>
class MathUtils
{
public:
	static double calPentagonArea(double side);
	static double calHexagonArea(double side);
};

