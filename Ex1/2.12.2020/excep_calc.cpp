#include <iostream>
#define INVALID 8200

int add2(int a, int b) {
    int sum = a + b;
    if (sum == INVALID)
    {
        throw 8200;
    }
    return sum;
}

int  multiply2(int a, int b) {
    int sum = 0;
    for (int i = 0; i < b; i++) {
        sum = add2(sum, a);
        if (sum == INVALID)
        {
            throw 8200;
        }
    };
    return sum;
}

int  pow2(int a, int b) {
    int exponent = 1;
    for (int i = 0; i < b; i++) {
        exponent = multiply2(exponent, a);
        if (exponent == INVALID)
        {
            throw 8200;
        }
    };
    return exponent;
}

int main(void) {
    try
    {
        std::cout << pow2(8201, 2) << std::endl;
        //std::cout << multiply2(4100, 2);
    }
    catch (int e)
    {
        std::cerr << "[ This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year ]\n";
        exit(0);
    }
    
}